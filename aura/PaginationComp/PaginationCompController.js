({
    // It will set all attributes nedded to redirect from current page to previous page whenever user click on previous button.
    // Then it will fire the event to provide all attributes values to the event handler component
	previousRecords : function(component, event, handler){
        var operationName="previousRecords";
        var currentPageNo=component.get("v.currentPageNo")-1;
        var accounts=component.get("v.accounts");
        var pageSize=component.get("v.pageSize");
        var start=component.get("v.start")-pageSize;
        var end=component.get("v.end")-pageSize;
        var totalSize=component.get("v.totalSize");
        var paginationList=[];
        var totalNoOfPagesArr=[];
        var startPageNo=component.get("v.startPageNo");
        var endPageNo=component.get("v.endPageNo");
        if(currentPageNo<startPageNo)
        {
            startPageNo=startPageNo-1;
            endPageNo=endPageNo-1;
            for(var i=startPageNo;i<=endPageNo;i++)
            {
                totalNoOfPagesArr.push(i);
            }
            component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);               
        }
        for(var i=start;i<end;i++)
        {
            paginationList.push(accounts[i]);
        }
        var paginationEventData=component.getEvent("paginationEventData");
        paginationEventData.setParams({"operationName":operationName,"currentPageNo":currentPageNo,"start":start,"end":end,"paginationList":paginationList,"totalNoOfPagesArr":totalNoOfPagesArr,"startPageNo":startPageNo,"endPageNo":endPageNo});
        paginationEventData.fire();
     },
    // It redirects to selected page when user click on any page no. in the pagination.
    // Then it will fire the event to provide all attributes values to the event handler component
    currentPageRecord : function(component, event, helper){
        var operationName="currentPageRecord";
        var currentPageNo=parseInt(event.target.id);
        var accounts=component.get("v.accounts");
        var pageSize=component.get("v.pageSize");
        var totalSize=component.get("v.totalSize");
        var end=pageSize*currentPageNo;
        var start=end-pageSize;
        var paginationList=[];
        var endPoint=end;
        if(end>totalSize){
            endPoint=totalSize;
        }
        for(var i=start;i < endPoint;i++)
        {
            paginationList.push(accounts[i]);
        }
        var paginationEventData=component.getEvent("paginationEventData");
        paginationEventData.setParams({"operationName" : operationName,"currentPageNo" : currentPageNo,"start" : start,"end" : end,"paginationList" : paginationList});
        paginationEventData.fire();
    },
    // It will set all attributes nedded to redirect from current page to next page whenever user click on next button.
    // Then it will fire the event to provide all attributes values to the event handler component
    nextRecords : function(component, event, handler){
        var operationName="nextRecords";
        var currentPageNo=component.get("v.currentPageNo")+1;
        var accounts=component.get("v.accounts");
        var end=component.get("v.end");
        var pageSize=component.get("v.pageSize");
        var totalSize=component.get("v.totalSize");
        var paginationList=[];
        var start=end;
        end=end+pageSize;
        var endPoint=end;
        var totalNoOfPagesArr=[];
        var startPageNo=component.get("v.startPageNo");
        var endPageNo=component.get("v.endPageNo");
        if(currentPageNo>endPageNo)
        {
            startPageNo=startPageNo+1;
            endPageNo=endPageNo+1;
            for(var i=startPageNo;i<=endPageNo;i++)
            {
                totalNoOfPagesArr.push(i);
            }
            component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);            
        }
        if(end>totalSize){
            endPoint=totalSize;
        }
        for(var i=start;i < endPoint;i++){
            paginationList.push(accounts[i]);
        }
        var paginationEventData=component.getEvent("paginationEventData");
        paginationEventData.setParams({"operationName":operationName,"currentPageNo":currentPageNo,"start":start,"end":end,"paginationList":paginationList,"totalNoOfPagesArr":totalNoOfPagesArr,"startPageNo":startPageNo,"endPageNo":endPageNo});
        paginationEventData.fire();
    },
})