({
    // It will get selectedFieldName and search key and then fire event to provide these attributes to event handler component.
    // And accounts list will be updated according to search criteria.
    onSelectChangeAndKeyUp : function(component, event, helper){
        var fieldName= component.find("selectedFieldName").get("v.value");
        var searchKey=component.get("v.searchRecord");
        console.log("fieldName : ",fieldName);
        console.log("searchKey : ",searchKey);
        var operationName="searchAndFieldFilter";
        var appEventDef=$A.get("e.c:appEvent");
        appEventDef.setParams({"fieldName":fieldName,"searchKey":searchKey,"operationName":operationName});
        appEventDef.fire();
    },
    // It will invoke remote method and initialize field name drop down according to field set.
    initializeAllFieldNames : function(component, event, helper){
        var allFieldsArr=[];
        var actionforAllFields=component.get("c.getAccountsWithAllFields");
        actionforAllFields.setCallback(this, function(resp){
            var state=resp.getState();
            if(state == "SUCCESS"){
                allFieldsArr=resp.getReturnValue();
                component.set("v.allFieldsArr",allFieldsArr);
            }
            else{
                console.log("state : ",state);
                console.log(resp);
            }
                
        });
        $A.enqueueAction(actionforAllFields);
    },
})