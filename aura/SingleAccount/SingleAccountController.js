({
    // It will be executed at the time of component initialization.
    // It will fetch all fields from field set.
    getAllFields : function(component, event, helper){
        var allFieldsArr=[];
        var actionforAllFields=component.get("c.getAccountsWithAllFields");
        actionforAllFields.setCallback(this, function(resp){
            var state=resp.getState();
            if(state == "SUCCESS"){
                allFieldsArr=resp.getReturnValue();
                component.set("v.allFieldsArr",allFieldsArr);
            }
            else{
                console.log("state : ",state);
                console.log(resp);
            }
        });
        $A.enqueueAction(actionforAllFields);
    },
    closeAlert : function(component,event,helper){
        component.set("v.showAlert", "false");
    },
    // It will be execute when user try to create new account record and validate,
    //  if Name is blank or not,record can not be created if user does not fill the name.
    //  And will fire the appEvent so that new created record can be added in to accounts list.
	createAcc : function(component, event, helper) {
        var acc=component.get("v.acc");
        if(acc == null || acc.Name.trim() == ""){
            component.set("v.showAlert", "true");
            return false;
        }
        var action=component.get("c.createUpdateAccount");
         action.setParams({
            "acc": acc
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log("state : "+state);
              	var acc = {"sobjectType":"Account","Name":"","AccountNumber":"","Phone":""};
       		    component.set("v.acc",acc);
                var operationName="newAccountCreated";
                var creatingAccountAppEvent=$A.get("e.c:appEvent");
                creatingAccountAppEvent.setParams({"account":response.getReturnValue(),"operationName":operationName});
                creatingAccountAppEvent.fire();
                component.set("v.isAccountChanged", !component.get("v.isAccountChanged"));
            }
            else{
                console.log("state : "+state);
                console.log(response);
            }
        });
		$A.enqueueAction(action);
	},
    // It will handle SingleFieldEditCompEvent event that will be fired,
    //  each time when a field value edited either in edit component or new record component.
    handleUpdateFieldRecorOnKeyUp : function(component, event, helper){
        var acc=component.get("v.acc");
        var fieldNewValue=event.getParam("fieldNewValue");
        var fieldNewName=event.getParam("fieldNewName");
        acc[fieldNewName]=fieldNewValue;
        component.set("v.acc",acc);
        event.stopPropagation();
    }
})