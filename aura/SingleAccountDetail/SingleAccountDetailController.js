({
	openModel: function(component, event, helper) {
      component.set("v.isOpen", true);
   },
    closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
      component.set("v.isEditRecordPopupOpen",false);
   },
   // It will be execute whenever we try to delete any record.
   // First it will close the popup modal and then will invoke server side controller's method to delete the record.
   // When record is deleted, it will fire the event so that event handler will update the accounts list.
   deleteRecord : function(component, event, helper) {
       component.set("v.isOpen", false);
       var acc=component.get("v.acc");
       var action=component.get("c.deleteAccountRecord");
       action.setParams({"acc":acc});
       action.setCallback(this, function(response){
           var state=response.getState();
           if(state == "SUCCESS"){
               var singleAcc=response.getReturnValue();
               console.log("state : "+state);
               console.log(singleAcc);
               var compEvent=component.getEvent("updateDeleteEvent");
               compEvent.setParams({"singleAcc" : singleAcc,"operationName" : "delete"});
               compEvent.fire();
           }
           else{
               console.log("state : "+state);
               console.log(response);
           }
       });
       $A.enqueueAction(action);
	},
   // It will open the popup modal for editing the record and set current record object into another object that will be updated to database.
   updateRecord : function(component, event, helper) {
       component.set("v.editRecordAccount",component.get("v.acc"));
       component.set("v.isEditRecordPopupOpen",true);
       
	},
   // It will close the popup modal,opened for editing the record and execute remote method to update edited record into database.
   // And then it will fire the event to update the pagination so that it remains on the same page where edited record is available.
   editAccount : function(component, event, helper){
        component.set("v.isEditRecordPopupOpen",false);
        var acc=component.get("v.editRecordAccount");
        var action=component.get("c.createUpdateAccount");
        action.setParams({"acc":acc});
        action.setCallback(this, function(response){
            var state=response.getState();
            if(state == "SUCCESS"){
                console.log("state : ",state);
                var singleAcc=response.getReturnValue();
                console.log(singleAcc);
                var compEvent=component.getEvent("updateDeleteEvent");
                compEvent.setParams({"singleAcc" : singleAcc,"operationName" : "update"});
                compEvent.fire();
            }
            else{
                console.log("state : ",state);
            }
        });
        $A.enqueueAction(action);
    },
    // It will update the editRecordAccount object each time when user makes changes in any field.
    handleUpdateFieldRecorOnKeyUp : function(component, event, helper){
        var editRecordAccount=component.get("v.editRecordAccount");
        var fieldNewValue=event.getParam("fieldNewValue");
        var fieldNewName=event.getParam("fieldNewName");
        editRecordAccount[fieldNewName]=fieldNewValue;
        component.set("v.editRecordAccount",editRecordAccount);
        event.stopPropagation();
    }
})