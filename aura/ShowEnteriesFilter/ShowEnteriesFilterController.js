({
    // It will set page size(how many records can be shown on a single page).
    // Then It will fire event to provide page size value to AccountDetailsComp
	pageSizeOnFilterChange : function(component, event, helper){
        var showEntrySize=parseInt(component.find("pageSizeFilter").get("v.value"));
        var appEventDef=$A.get("e.c:appEvent");
        var operationName="showEntryFilter";
        appEventDef.setParams({"showEntrySize":showEntrySize,"operationName":operationName});
        appEventDef.fire();
    }
})