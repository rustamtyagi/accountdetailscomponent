({
    // this method will be executed at the time of component initialization.
    // first it will fetch all fields from field set then it will fetch all account records from the database.
    // And then will set all attribute values to show records in the form of pagination
	initialMethod : function(component, event, helper) {
        var pageSize = component.get("v.pageSize");
        var actionforAllFields=component.get("c.getAccountsWithAllFields");
        actionforAllFields.setCallback(this, function(resp){
            var state=resp.getState();
            if(state == "SUCCESS"){
                var allFieldsArr=resp.getReturnValue();
                component.set("v.allFieldsArr",allFieldsArr);
                console.log("allFieldsArr : length : ",allFieldsArr.length)
            }
            else{
                console.log("state : ",state);
                console.log(resp);
            }
                
        });
        $A.enqueueAction(actionforAllFields);
       
        var action=component.get("c.getAccountsWithFields");
        action.setCallback(this,function(response){
            var state=response.getState();
            if (component.isValid() && state == "SUCCESS") 
            {
                component.set("v.accounts", response.getReturnValue());
                component.set("v.start", 0);
                var accounts = response.getReturnValue();
                component.set("v.totalSize", accounts.length);
                component.set("v.end",pageSize);
                var totalSize=accounts.length;
                var result=parseInt(totalSize/pageSize);
                var mod=totalSize % pageSize;
                var totalNoOfPages=1;
                var totalNoOfPagesArr=[];
                if(mod == 0)
                {
                    totalNoOfPages=result;
                }
                else
                {
                    totalNoOfPages=result+1;
                }
                var startPageNo=1;
                var endPageNo=5;
                if(totalNoOfPages < 5)
                    endPageNo=totalNoOfPages;
                component.set("v.startPageNo",startPageNo);
                component.set("v.endPageNo",endPageNo);  
                for(var i=startPageNo;i<=endPageNo;i++)
                {
                    totalNoOfPagesArr.push(i);
                }
            	component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
                var start=component.get("v.start");
                var end=component.get("v.end");
                var paginationList=[];
                for(var i = start;i < end; i++)
                {
                    if(accounts[i] != null)
                    paginationList.push(accounts[i]);
                }
                component.set("v.paginationList", paginationList);
            }
            else
            {
                console.log("state failed::::: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    // It will be executed to handle SingleCompEvent event when any record is edited,deleted.
    // It will update the accounts and paginationlist attribute acording to operation.
    UpdateDeleteAction : function(component, event, helper){
        var acc=event.getParam("singleAcc");
        var operationName=event.getParam("operationName");
        var accounts=component.get("v.accounts");
        var newAccountRecrods=[];
        for(var i=0;i<accounts.length;i++){
            if(operationName == "delete"){
                if(accounts[i].Id != acc.Id)
                    newAccountRecrods.push(accounts[i]);
            }
            if(operationName == "update"){
                if(accounts[i].Id != acc.Id)
                    newAccountRecrods.push(accounts[i]);
                else
                    newAccountRecrods.push(acc);
            }
        }
        component.set("v.accounts",newAccountRecrods);
        if(operationName == "update"){
            var accounts=newAccountRecrods;
            var start=component.get("v.start");
            var end=component.get("v.end");
            var paginationList=[];
            for(var i = start;i < end; i++)
            {
                if(accounts[i] != null)
                    paginationList.push(accounts[i]);
            }
            component.set("v.paginationList", paginationList);
        }
        if(operationName == "delete"){
            component.set("v.currentPageNo",1);
            var accounts=newAccountRecrods;
            component.set("v.totalSize",accounts.length);
            var pageSize=component.get("v.pageSize");
            component.set("v.start", 0);
            component.set("v.end", pageSize);
            var start=component.get("v.start");
            var end=component.get("v.end");
            var paginationList=[];
            var totalSize=component.get("v.totalSize");
            var result=parseInt(totalSize/pageSize);
            var mod=totalSize % pageSize;
            var totalNoOfPages=1;
            var totalNoOfPagesArr=[];
            if(mod == 0)
            {
                totalNoOfPages=result;
            }
            else
            {
                totalNoOfPages=result+1;
            }
            var startPageNo=1;
            var endPageNo=5;
            if(totalNoOfPages < 5)
                endPageNo=totalNoOfPages;
            component.set("v.startPageNo",startPageNo);
            component.set("v.endPageNo",endPageNo);  
            for(var i=startPageNo;i<=endPageNo;i++)
            {
                totalNoOfPagesArr.push(i);
            }
            component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
            var endPoint=end;
            if(end>totalSize){
                endPoint=totalSize;
            }
            for(var i=start;i < endPoint;i++)
            {
                paginationList.push(accounts[i]);
            }
            component.set("v.paginationList",paginationList);
        }
        console.log("newAccountRecrods length : ",newAccountRecrods.length);
    },
    // It will handle pagination event and update the attrubutes accordingly.
    handlePaginationEvent : function(component, event, handler){
        
        var operationName=event.getParam("operationName");
        var currentPageNo=event.getParam("currentPageNo");
        var start=event.getParam("start");
        var end=event.getParam("end");
        var paginationList=event.getParam("paginationList");
        component.set("v.currentPageNo",currentPageNo);
        component.set("v.start",start);
        component.set("v.end",end);
        component.set("v.paginationList",paginationList);
        if(operationName != "currentPageRecord"){
            var totalNoOfPagesArr=event.getParam("totalNoOfPagesArr");
            var startPageNo=event.getParam("startPageNo");
            var endPageNo=event.getParam("endPageNo");
            component.set("v.startPageNo",startPageNo);
            component.set("v.endPageNo",endPageNo);
            console.log("currentPageNo : ",currentPageNo);
            console.log("startPageNo : ",startPageNo);
            console.log("endPageNo : ",endPageNo);
            
            if(operationName == "previousRecords" && currentPageNo<startPageNo)
                component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
            if(operationName == "nextRecords" && currentPageNo>endPageNo)
                component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
        }
    },
    // It will handle the application event which is fired when we try to search the records from search criteria
    //  and when a new account is created .
    //  It will update all the attributes accordingly.
    handleAppEvent : function(component, event, handler){
        var operationName=event.getParam("operationName");
        console.log("operationName : ",operationName);
        if(operationName == "searchAndFieldFilter"){
            var fieldName=event.getParam("fieldName");
            var searchKey=event.getParam("searchKey");
            
            var action=component.get("c.searchAccountRecords");
            action.setParams({"searchKey":searchKey,"fieldName":fieldName});
            action.setCallback(this, function(response){
                var state=response.getState();
                if(state == "SUCCESS")
                {
                    console.log("state : ",state);
                    component.set("v.currentPageNo",1);
                    component.set("v.accounts", response.getReturnValue());
                    var accounts=component.get("v.accounts");
                    component.set("v.totalSize",accounts.length);                
                    var pageSize=component.get("v.pageSize");
                    component.set("v.start", 0);
                    component.set("v.end", pageSize);
                    var start=component.get("v.start");
                    var end=component.get("v.end");
                    var paginationList=[];
                    var totalSize=component.get("v.totalSize");
                    var endPoint=end;
                    var result=parseInt(totalSize/pageSize);
                    var mod=totalSize % pageSize;
                    var totalNoOfPages=1;
                    var totalNoOfPagesArr=[];
                    if(mod == 0)
                    {
                        totalNoOfPages=result;
                    }
                    else
                    {
                        totalNoOfPages=result+1;
                    }
                    var startPageNo=1;
                    var endPageNo=5;
                    if(totalNoOfPages < 5)
                        endPageNo=totalNoOfPages;
                    component.set("v.startPageNo",startPageNo);
                    component.set("v.endPageNo",endPageNo);  
                    for(var i=startPageNo;i<=endPageNo;i++)
                    {
                        totalNoOfPagesArr.push(i);
                    }
                    component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
                    if(end>totalSize){
                        endPoint=totalSize;
                    }
                    for(var i=start;i < endPoint;i++)
                    {
                        paginationList.push(accounts[i]);
                    }
                    component.set("v.paginationList",paginationList);
                }
                else
                {
                    console.log("state : ",state);
                    console.log(response);
                }
            });
            $A.enqueueAction(action);
            
        }
        else if(operationName == "showEntryFilter"){
            var showEntrySize=event.getParam("showEntrySize");
            console.log("showEntrySize: "+showEntrySize);
            
            component.set("v.currentPageNo",1);
            var accounts=component.get("v.accounts");
            component.set("v.pageSize",parseInt(showEntrySize));
            var pageSize=component.get("v.pageSize");
            component.set("v.start", 0);
            component.set("v.end", pageSize);
            var start=component.get("v.start");
            var end=component.get("v.end");
            var paginationList=[];
            var totalSize=component.get("v.totalSize");
            var endPoint=end;
            var result=parseInt(totalSize/pageSize);
            var mod=totalSize % pageSize;
            var totalNoOfPages=1;
            var totalNoOfPagesArr=[];
            if(mod == 0)
            {
                totalNoOfPages=result;
            }
            else
            {
                totalNoOfPages=result+1;
            }
            var startPageNo=1;
            var endPageNo=5;
            if(totalNoOfPages < 5)
                endPageNo=totalNoOfPages;
            component.set("v.startPageNo",startPageNo);
            component.set("v.endPageNo",endPageNo);  
            for(var i=startPageNo;i<=endPageNo;i++)
            {
                totalNoOfPagesArr.push(i);
            }
            component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
            if(end>totalSize){
                endPoint=totalSize;
            }
            for(var i=start;i < endPoint;i++)
            {
                paginationList.push(accounts[i]);
            }
            component.set("v.paginationList",paginationList);
            
            
        }
        else if(operationName == "newAccountCreated"){
            var acc=event.getParam("account");
            console.log("operationName ....: ",operationName);
            console.log(acc.Name);
            
            var accounts=component.get("v.accounts");
            var newAccountRecrods=[];
            newAccountRecrods.push(acc);
            for(var i=0;i<accounts.length;i++){
                
                newAccountRecrods.push(accounts[i]);
            }
            component.set("v.accounts",newAccountRecrods);
            component.set("v.currentPageNo",1);
            accounts=newAccountRecrods;
            component.set("v.totalSize",accounts.length);
            var pageSize=component.get("v.pageSize");
            component.set("v.start", 0);
            component.set("v.end", pageSize);
            var start=component.get("v.start");
            var end=component.get("v.end");
            var paginationList=[];
            var totalSize=component.get("v.totalSize");
            var result=parseInt(totalSize/pageSize);
            var mod=totalSize % pageSize;
            var totalNoOfPages=1;
            var totalNoOfPagesArr=[];
            if(mod == 0)
            {
                totalNoOfPages=result;
            }
            else
            {
                totalNoOfPages=result+1;
            }
            var startPageNo=1;
            var endPageNo=5;
            if(totalNoOfPages < 5)
                endPageNo=totalNoOfPages;
            component.set("v.startPageNo",startPageNo);
            component.set("v.endPageNo",endPageNo);  
            for(var i=startPageNo;i<=endPageNo;i++)
            {
                totalNoOfPagesArr.push(i);
            }
            component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
            var endPoint=end;
            if(end>totalSize){
                endPoint=totalSize;
            }
            for(var i=start;i < endPoint;i++)
            {
                paginationList.push(accounts[i]);
            }
            component.set("v.paginationList",paginationList);
        }
    },
    // It will be execute when user search any record according to searchkey and field name, and will update the records after search.
    onSearchKeyUp : function(component, event, helper){
		var fieldName=component.get("v.clickedFieldName");
        var action=component.get("c.searchAccountRecords");
        action.setParams({"searchKey":searchKey,"fieldName":fieldName});
        action.setCallback(this, function(response){
            var state=response.getState();
            if(state == "SUCCESS")
            {
                component.set("v.currentPageNo",1);
                component.set("v.accounts", response.getReturnValue());
                var accounts=component.get("v.accounts");
                component.set("v.totalSize",accounts.length);                
                var pageSize=component.get("v.pageSize");
                component.set("v.start", 0);
                component.set("v.end", pageSize);
                var start=component.get("v.start");
                var end=component.get("v.end");
                var paginationList=[];
                var totalSize=component.get("v.totalSize");
                var endPoint=end;
                var result=parseInt(totalSize/pageSize);
                var mod=totalSize % pageSize;
                var totalNoOfPages=1;
                var totalNoOfPagesArr=[];
                if(mod == 0)
                {
                    totalNoOfPages=result;
                }
                else
                {
                    totalNoOfPages=result+1;
                }
                var startPageNo=1;
                var endPageNo=5;
                if(totalNoOfPages < 5)
                    endPageNo=totalNoOfPages;
                component.set("v.startPageNo",startPageNo);
                component.set("v.endPageNo",endPageNo);  
                for(var i=startPageNo;i<=endPageNo;i++)
                {
                    totalNoOfPagesArr.push(i);
                }
                component.set("v.totalNoOfPagesArr",totalNoOfPagesArr);
                if(end>totalSize){
                    endPoint=totalSize;
                }
                for(var i=start;i < endPoint;i++)
                {
                    paginationList.push(accounts[i]);
                }
                component.set("v.paginationList",paginationList);
            }
            else
            {
                console.log(state);
                console.log(response);
            }
        });
        $A.enqueueAction(action);  
    }
})