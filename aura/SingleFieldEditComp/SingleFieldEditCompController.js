({
    // It will set field's label of a field for a record.
	doInit : function(component, event, helper) {
		var account=component.get("v.account");
        var fieldName=component.get("v.fieldName");
        component.set("v.fieldValue",account[fieldName]);
	},
    // it will fetch field's label and value and fire the event.
    // And event handler component will update the record's fields.
    updateRecordField : function(component, event, helper){
        var fieldValue=component.get("v.fieldValue");
        var fieldName=component.get("v.fieldName");
        var updateFieldRecorOnKeyUp=component.getEvent("updateFieldRecorOnKeyUp");
        updateFieldRecorOnKeyUp.setParams({"fieldNewName":fieldName,"fieldNewValue":fieldValue});
        updateFieldRecorOnKeyUp.fire();
    }
})