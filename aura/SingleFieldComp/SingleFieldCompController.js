({
    // It sets a field value according to field name of that account object.
	doInit : function(component, event, helper) {
		var acc=component.get("v.acc");
        var fieldName=component.get("v.fieldName");
        component.set("v.fieldValue",acc[fieldName]);
	}
})