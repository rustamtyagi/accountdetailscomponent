/*
	Name: AccountInfoController
    Description: Its a server side controller for Account Details component that perform all SOQL and DML operations like fetching all records,
		delete or edit a purticular record and fetch all the fields from field set and showing every where like in view,edit,creating new record,field filter criteria. 
	Created By: Rustam Tyagi
	class level fields : allFields,allFieldsArr
	allFields : its a static variable that contains all fields of fieldset as string seprated by comma(,).
	allFieldsArr : contains all fields of fieldset in the form of String array.
	Note : This controller perform operations on Account object.
*/
public class AccountInfoController 
{
    public static String allFields='';
    public static String[] allFieldsArr;
    /*
    	Name: getAccountsWithAllFields
		Params: non-parametrized(It does not take any parameter as argument).
		Functionality : It returns array of String of fieldset for component UI purpose and sets all fields in allFields variable to be used in the construction of query.
		Note : AccountFieldSet is field set name,if some how it does not contain any element or contains only Id as field or it would be deleted then component will not stop its functioning and it will show fields -- Name as default feild.
    */
    @AuraEnabled
    public static String[] getAccountsWithAllFields(){
        // Account is object and AccountFieldSet is fieldset name
        allFields='';
        List<Schema.FieldSetMember> allFieldsMember;
        try{
            allFieldsMember=SObjectType.Account.FieldSets.AccountFieldSet.getFields();
            if(allFieldsMember != null && allFieldsMember.size() > 0){
                Boolean fieldSetContainsName=false;
                for(Schema.FieldSetMember f : allFieldsMember)
                {
                    if(f.getFieldPath() == 'Name')
                        fieldSetContainsName=true;
                    allFields+=f.getFieldPath() + ',';
                }
                if(fieldSetContainsName == false)
                    allFields='Name,'+allFields;
                allFields=allFields.removeEnd(',');
            }
            else{
                allFields='Name';
        }
        }catch(Exception excep){
            System.debug('exception caused while getting AccountFieldSet field set : '+excep.getMessage());
            allFields='Name';
        }
        allFieldsArr=allFields.split(',');
        return allFieldsArr;
    }
    /*
    	Name: getAccountsWithFields
		Params: non-parametrized(It does not take any parameter as argument).
		Functionality : It returns List of Accounts according to fieldset.
    */
    @AuraEnabled
    public static List<Account> getAccountsWithFields(){
        String query = 'SELECT '+allFields+' FROM Account  order by CreatedDate desc';
        return Database.query(query);
    }
    /*
    	Name: searchAccountRecords
		Params: searchKey,fieldName.
		Functionality : It contains logic to construct dynamic query and returns List of Accounts according to search criteria.
    */
    @AuraEnabled
    public static List<Account> searchAccountRecords(String searchKey,String fieldName){
        List<Schema.FieldSetMember> allFieldsMember;
        try{
            allFieldsMember=SObjectType.Account.FieldSets.AccountFieldSet.getFields();
            if(allFieldsMember != null && allFieldsMember.size() > 0){
                Boolean fieldSetContainsName=false;
                for(Schema.FieldSetMember f : allFieldsMember)
                {
                    if(f.getFieldPath() == 'Name')
                        fieldSetContainsName=true;
                    allFields+=f.getFieldPath() + ',';
                }
                if(fieldSetContainsName == false)
                    allFields='Name,'+allFields;
                allFields=allFields.removeEnd(',');
            }
            else{
                allFields='Name';
        }
        }catch(Exception excep){
            System.debug('exception caused while getting AccountFieldSet field set : '+excep.getMessage());
            allFields='Name';
        }
        searchKey='%'+searchKey+'%';
        String query='select '+allFields+' from Account where '+fieldName+' like \''+searchKey+'\''+' order by CreatedDate desc';
        System.debug('query : '+query);
        return Database.query(query);
    }
    /*
    	Name: deleteAccountRecord
		Params: acc(account object that needs to be deleted)
		Functionality : It returns the same account object which has been deleted.
    */
    @AuraEnabled
    public static Account deleteAccountRecord(Account acc){
        if(acc != null)
        	delete acc;
        return acc;
    }
    /*
    	Name: deleteAccountRecord
		Params: acc(account object that needs to be created or updated into Database)
		Functionality : It returns the same account object which has been created or updated.
    */
    @AuraEnabled
    public static Account createUpdateAccount(Account acc){ 
        if(acc != null)
            upsert acc;
        return acc;
        
    }
}